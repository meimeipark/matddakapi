package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.MoneyHistory;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.moneyHistory.MoneyType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoneyHistoryRepository extends JpaRepository<MoneyHistory, Long> {

    /**
     * 라이더 1명 입금 or 출금 내역 전체 보기 최신순
     */
    List<MoneyHistory> findByRider_IdAndMoneyTypeOrderByIdDesc(long id1, MoneyType moneyType);

    /**
     * 라이더 전체 입금 or 출금 내역 전체 보기 최신순 페이징
     * @param moneyType 입금 or 출금
     * @return
     */
    Page<MoneyHistory> findAllByMoneyTypeOrderByIdDesc(MoneyType moneyType, Pageable pageable);
}
