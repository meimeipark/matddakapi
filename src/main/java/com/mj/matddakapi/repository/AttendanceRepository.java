package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.model.attendance.AttendanceResponse;
import org.apache.coyote.Response;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    /**
     * 근태 단수용
     */
    Optional<Attendance> findByRider_Id(long riderId);
}
