package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Income;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface IncomeRepository extends JpaRepository<Income, Long> {

    /**
     * 실적 복수 최신순 페이징
     */
    Page<Income> findAllByOrderByIdDesc(Pageable pageable);

    /**
     * 하루 간의 데이터
     */
    List<Income> findAllByDateIncome(LocalDate dateIncome);

    /**
     * 라이더 1명의 하루 간의 데이터
     */
    List<Income> findAllByDateIncomeAndDelivery_Rider_Id(LocalDate dateIncome, long riderId);

    /**
     * 일정 기간의 데이터
     * @param startDate
     * @param endDate
     * @return
     */
    List<Income> findAllByDateIncomeBetween(LocalDate startDate, LocalDate endDate);

    /**
     * 라이더 1명의 일정 기간의 데이터
     * @param startDate
     * @param endDate
     * @return
     */
    List<Income> findAllByDateIncomeBetweenAndDelivery_Rider_Id(LocalDate startDate, LocalDate endDate, long riderId);
}
