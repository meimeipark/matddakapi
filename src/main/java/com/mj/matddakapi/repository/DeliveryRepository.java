package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Delivery;
import com.mj.matddakapi.enums.delivery.StateDelivery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    // 배차용
    Optional<Delivery> findByAsk_Id(Long askId);

    // 배송 복수 최신순
    List<Delivery> findAllByOrderByIdDesc();

    // 배송 복수 최신순 페이징
    Page<Delivery> findAllByOrderByIdDesc(Pageable pageable);

    // 배송 복수 상태 같으면서 최신순
    List<Delivery> findAllByStateDeliveryOrderByIdDesc(StateDelivery stateDelivery);

    // 배송 복수 상태 같으면서 최신순 페이징
    Page<Delivery> findAllByStateDeliveryOrderByIdDesc(StateDelivery stateDelivery, Pageable pageable);
}
