package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Board;
import com.mj.matddakapi.model.board.BoardItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BoardRepository extends JpaRepository<Board, Long> {

    /**
     * 게시글 최신순
     */
    List<Board> findAllByOrderByIdDesc();

    /**
     * 게시글 최신순 페이징
     */
    Page<Board> findAllByOrderByIdDesc(Pageable pageable);
}
