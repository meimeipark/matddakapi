package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Rider;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RiderRepository extends JpaRepository<Rider, Long> {
    long countByUsername(String username);
    Optional<Rider> findByUsernameAndPassword(String username,String password);

    List<Rider> findAllByIsBan(Boolean isBan);

    Optional<Rider> findByUsername(String username);
}