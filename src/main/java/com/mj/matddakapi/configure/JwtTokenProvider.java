package com.mj.matddakapi.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;
    @Value("${spring.jwt.secret}")
    private String secretKey;

    // Post(먼저) Construct(구조)
    @PostConstruct
    // 암호화기법 Base64(단방향)
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    // 토큰 생성
    // claim 이라는 단어 뜻 확인
    public String createToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("role", role);
        // 토큰은 Date (미국 시간) 기준으로
        Date now = new Date();
        // 토큰 유효시간
        // 1000 밀리 세컨드 = 1 초
        // 기본적으로 10시간 유효하게 설정해준다. 아침에 출근해서 로그인하고 점식 먹고 퇴근 시간 약 10시간
        // 앱용 토큰 같은 경우 유효시간 1년으로 설정해 준다. 앱에서 아침마다 로그인하기 귀찮
        // 프론트에서 쿠기도 동일하게 web은 10시간 설정(동일하지 않을 경우 의미가 없다)
        long tokenValidMillisecond = 1000L * 60 * 60 * 10;
        if (type.equals("APP")) tokenValidMillisecond = 1000L * 60 * 60 * 24 * 365;
        // 토큰 생성해서 리턴
        // jwt 사이트 참고
        // 유효시간도 넣어준다. 생성요청한 시간 ~ 현재 + 위에서 설정된 유효초 만큼
        return Jwts.builder()
                // 나야
                .setClaims(claims)
                // 언제 발급되었다.
                .setIssuedAt(now)
                // 만료일(언제부터 언제까지 사용 가능)
                .setExpiration(new Date(now.getTime() + tokenValidMillisecond))
                // 암호화
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    // 토큰을 분석하여 인증정보를 가져옴
    // 일회용 출입증
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    // 토큰을 파싱하여 username을 가져옴
    // 토큰 생성 시 username은 subject에 넣은 것 꼭 확인.
    // jwt 사이트 보면서 코드 이해
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    //resolve라는 단어 확인
    // promise resolve 성공을 해야 주는 값(성공 결과물)
    // 성공한 토큰
    public String resolveToken(HttpServletRequest request){
        // rest api - header 인증 방식에서 Bearer를 언제 사용하는지를 봐야한다.
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    // 유효시간을 검사하여 유효시간이 지났으면 false를 줌
    public boolean validateToken (String jwtToken) {
        // try catch문은 틀려도 실행이 되며, 문제가 생기면 throw로 던진다.
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}
