package com.mj.matddakapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
// 시작할때 필터를 낀다
public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;
    @Override
    // 고객 -> 서버한테 주는 값 request, 서버 -> 고객한테 주는 값 response, 걸러주는 필터끼리 엮어준다(필터 단계설정)
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // provider 가 제공해주는 resolveToken
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        // resolve 값이 없거나, Bearer로 시작하지 않는 값일 경우 빠져나간다
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")){
            chain.doFilter(request, response);
            return;
        }
        // 양식은 제대로 되었으나 띄어쓰기(split) 기준 첫번째 요소를 기준으로 앞 뒤 공백을 제거한다
        token = tokenFull.split(" ")[1].trim();
        // 푸싱파싱한 값을 검증 하지 못했을 경우 빠져나간다.
        if (!jwtTokenProvider.validateToken(token)){
            chain.doFilter(request,response);
            return;
        }
        //일회용 출입증을 발급한다.
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        // Context(환경)
        // 보안환경고정(일회용 출입증을 고정, 요청에 따라)
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}
