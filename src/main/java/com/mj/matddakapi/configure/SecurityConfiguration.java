package com.mj.matddakapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    // 시작할 때 사용
    @Bean
    public CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        // 모두 허용하겠다
        config.setAllowedOrigins(List.of("*"));
        // 메소드 매핑을 허용하겠다. 어떤 매핑이든 api 두번실행(사전요청:프리플라이트, 실제실행) (options-> 프리플라이트)
        config.setAllowedMethods(List.of("GET", "POST", "PUSH", "DELETE", "PATCH", "OPTIONS"));
        // 모든 헤더 요청 받아주기
        config.setAllowedHeaders(List.of("*"));
        // 모든 노출된 헤더 허용
        config.setExposedHeaders(List.of("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // 모든 메핑주소는 CorsConfiguration 정책으로 설정하겠다.
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    //flutter 에서 header에 token 넣는 법
    //String? token = await TokenLib.getToken();
    //Dio dio = Dio();
    //dio.options.headers['Authorization'] = ''Bearer ' + token!; <-- 한줄 추가

    //vue.js에서 axios에 header 넣는 법
    //https://velog.io/@ch9eri/Axios-headers-Authorization

    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // csrf: Cross-Site Request Forgery 사이트 간 요청 위조
        http.csrf(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                // 설정한 CorsConfiguration 정책을 사용하게 끔 chain으로 묶는다.
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource()))
                .sessionManagement((sessionManagement) ->
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                // 요청을 인가하다
                .authorizeHttpRequests((authorizeRequests) ->
                        authorizeRequests
                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll()
                                // 먼저 허용해준다
                                .requestMatchers(HttpMethod.GET, "/exception/**").permitAll()
                                // 멤버 로그인은 모두 허용해준다
                                .requestMatchers("/v1/rider/login/**").permitAll()
                                // 역할 별로 허용되는 페이지를 넣어준다.
                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN", "MEMBER")
                                .requestMatchers("/v1/rider/all").hasAnyRole("ADMIN", "MEMBER")
                                .anyRequest().hasRole("ADMIN")
                );
        // 예외 시 핸들러한테 넘겨서 메시지가 나올 수 있게 처리
        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied));
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));
        // 토큰 프로바이더한테 필터의 자리
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }


}
