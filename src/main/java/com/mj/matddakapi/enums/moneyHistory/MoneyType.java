package com.mj.matddakapi.enums.moneyHistory;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MoneyType {
    IN("입금")
    ,OUT("출금");

    private final String name;
}
