package com.mj.matddakapi.enums.attendance;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StateWork {
    STRAT_WORK("출근")
    ,PAUSE_WORK("휴식")
    ,RESTART_WORK("휴식끝")
    ,STOP_WORK("퇴근");

    private final String name;
}
