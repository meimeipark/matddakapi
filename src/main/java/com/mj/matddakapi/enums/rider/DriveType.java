package com.mj.matddakapi.enums.rider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DriveType {
    CARRY("자동차")
    , MOTORCYCLE("오토바이")
    , WALKING("도보")
    , ELECTRIC_BICYCLE("전기 자전거")
    , CYCLE("자전거")
    , ELECTRIC_SCOOTER("전동 킥보드");

    private final String name;
}
