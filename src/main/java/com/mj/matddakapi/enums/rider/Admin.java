package com.mj.matddakapi.enums.rider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Admin {
    // string은 정해진 형식이 없기 때문에 권한이다 아니다를 알려주기 위해서 ROLE을 앞에 붙인다.
    ROLE_ADMIN("관리자"),
    ROLE_NORMAL("일반회원");

    private final String name;
}
