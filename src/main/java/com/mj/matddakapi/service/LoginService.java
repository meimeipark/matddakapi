package com.mj.matddakapi.service;

import com.mj.matddakapi.configure.JwtTokenProvider;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.rider.login.LoginRequest;
import com.mj.matddakapi.model.rider.login.LoginResponse;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Member;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final RiderRepository riderRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;


// 기존 코드
//    public SingleResult<LoginResponse> doLogin(LoginRequest request) {
//        // 아이디와 비밀번호가 일치하는 데이터 가져온다
//        Optional<Rider> rider = riderRepository.findByUsernameAndPassword(
//                request.getPassword(),
//                request.getUsername()
//        );
//        if (rider.isPresent()){
//            LoginResponse loginResponse = new LoginResponse();
//            loginResponse.setRiderId(rider.get().getId());
//            loginResponse.setRiderUsername(rider.get().getUsername());
//
//            SingleResult<LoginResponse> result = new SingleResult<>();
//            result.setData(loginResponse);
//            result.setCode(0);
//            result.setMsg("로그인 성공");
//
//            return result;
//
//        }else {
//            SingleResult<LoginResponse> result = new SingleResult<>();
//            result.setCode(-1);
//            result.setMsg("아이디와 패스워드를 확인해주세요");
//
//            return result;
//        }
//    }

    // 로그인 타입은 WEB or APP (WEB인 경우 토큰 유효시간 10시간, APP은 1년) -> JwtTokenProvider
    public LoginResponse doLogin(Admin admin, LoginRequest loginRequest, String loginType) {
        Rider rider = riderRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CRiderMissingException::new);
        // 회원 정보가 없습니다. 던지기

        if (!rider.getAdmin().equals(admin)) throw new CRiderMissingException();
        // 일반 회원이 최고 관리자용으로 로그인하려는 경우로 메세지는 회원정보가 없습니다로 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), rider.getPassword())) throw new CRiderMissingException();
        // 비밀번호가 일치하지 않습니다. 던지기

        String token = jwtTokenProvider.createToken(String.valueOf(rider.getUsername()), rider.getAdmin().toString(), loginType);

        return new LoginResponse.Builder(token, rider.getName()).build();
    }
}
