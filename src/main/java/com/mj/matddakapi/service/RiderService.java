package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.enums.attendance.StateWork;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.enums.rider.ReasonBan;
import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.lib.CommonCheck;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.rider.*;
import com.mj.matddakapi.model.rider.person.RiderBankChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import com.mj.matddakapi.repository.AttendanceRepository;
import com.mj.matddakapi.repository.RiderPayRepository;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor

public class RiderService {
    private final RiderRepository riderRepository;
    private final PasswordEncoder passwordEncoder;
    private final AttendanceRepository attendanceRepository;
    private final RiderPayRepository riderPayRepository;

    public Rider getData(long riderId) {
        return riderRepository.findById(riderId).orElseThrow();
    }

    /**
     * @param username 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복확인했을 때 데이터베이스에서 일치하는 이메일이 없을 경우
     */
    private boolean isNewRider(String username) {
        long dupCount = riderRepository.countByUsername(username);

        return dupCount <= 0;
    }

    // 관리자는 관리자만 등록가능해야하고 일반회원은 아무나 등록 가능해야한다.
    // 아무나 관리자로 등록가능하면 안된다.
    // 컨트롤러에서 일반회원가입, 관리자 등록 두개로 나눠서 구현해야한다.
    // 일반회원가입 서비스 따로, 관리자 등록 서비스 따로하지 않고 비슷한 코드이기 때문에 굳이 나누지 않는다.
    // 어떤 회원 그룹에 가입시킬 것인지 서비스에서 받고
    public void setRider(Admin admin, RiderJoinRequest joinRequest) {
        if (!CommonCheck.isValidEmail(joinRequest.getUsername())) throw new CRiderMissingException(); // 유효한 아이디 형식이 아닙니다. 던지기
        if (!joinRequest.getPassword().equals(joinRequest.getPasswordRe())) throw new CRiderMissingException(); // 비밀번호가 일치하지 않습니다. 던지기
        if (!isNewRider(joinRequest.getUsername()))throw new CRiderMissingException(); // 중복된 아이디가 존재합니다. 던지기

        joinRequest.setPassword(passwordEncoder.encode(joinRequest.getPassword()));
        Rider rider = new Rider.Builder(admin, joinRequest).build();
        riderRepository.save(rider);
    }

//    public void setRider(RiderJoinRequest request) throws Exception {
//        // 중복확인 로직
//        if (!isNewRider(request.getUsername())) throw new Exception();
//        Rider addData1 = new Rider.Builder(request).build();
//        riderRepository.save(addData1);
//
//        Attendance addData2 = new Attendance();
//        addData2.setRider(addData1);
//        addData2.setStateWork(StateWork.STOP_WORK);
//        addData2.setDateCheckWork(LocalDateTime.now());
//
//        attendanceRepository.save(addData2);
//
//        RiderPay addData3 = new RiderPay();
//        addData3.setRider(addData1);
//        addData3.setPayNow(0D);
//
//        riderPayRepository.save(addData3);
//    }

    /**
     * 관리자(웹) 회원 리스트 조회
     */
    public List<RiderItem> getRiders() {
        List<Rider> originList = riderRepository.findAll();
        List<RiderItem> result = new LinkedList<>();

        for (Rider rider : originList)
            result.add(new RiderItem.Builder(rider).build());

        return result;//
//        Attendance addData2 = new Attendance();
//        addData2.setRider(addData1);
//        addData2.setStateWork(StateWork.STOP_WORK);
//        addData2.setDateCheckWork(LocalDateTime.now());
//
//        attendanceRepository.save(addData2);
//
//        RiderPay addData3 = new RiderPay();
//        addData3.setRider(addData1);
//        addData3.setPayNow(0D);
//
//        riderPayRepository.save(addData3);
//    }
    }

    /**
     * 관리자, 라이더: 회원 정보 조회 detail
     */
    public RiderResponse getRider(long id) {
        Rider originData = riderRepository.findById(id).orElseThrow();

        return new RiderResponse.Builder(originData).build();
    }

    /**
     * 관리자: 라이더 페이징 1~10페이지
     */
    public ListResult<RiderItem> getPages(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 9);
        Page<Rider> riderPages = riderRepository.findAll(pageRequest);

        List<RiderItem> result = new LinkedList<>();

        for (Rider rider : riderPages.getContent())
            result.add(new RiderItem.Builder(rider).build());


        ListResult<RiderItem> result1 = new ListResult<>();
        result1.setList(result);
        result1.setTotalCount(riderPages.getTotalElements());
        result1.setTotalPage(riderPages.getTotalPages());
        result1.setCurrentPage(riderPages.getPageable().getPageNumber());

        return result1;
    }
    /**
     * 관리자: 회원정보 수정
     */
    public void putRiders(long id, RiderChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putRiders(request);

        riderRepository.save(originData);
    }

    /**
     * 라이더: 개인정보 수정
     */
    public void putRider(long id, RiderPersonChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putRider(request);

        riderRepository.save(originData);
    }
    /**
     * 라이더: 계좌 수정
     */
    public void putBank(long id, RiderBankChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putBank(request);

        riderRepository.save(originData);
    }
    /**
     * 라이더: 운송수단 변경
     */
    public void putRidingType(long id, RiderTypeChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putRidingType(request);

        riderRepository.save(originData);
    }
    public List<RiderItem> getBlackList(){
        List<Rider> originList = riderRepository.findAllByIsBan(true);
        List<RiderItem> result = new LinkedList<>();
        for (Rider rider : originList)
            result.add(new RiderItem.Builder(rider).build());

        return result;
    }
}
