package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final RiderRepository riderRepository;

    @Override
    // 권한을 가진 회원을 어떻게 확인할 것인지
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Rider rider = riderRepository.findByUsername(username).orElseThrow(CRiderMissingException::new);
        return rider;
    }
}
