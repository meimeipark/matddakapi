package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Income;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.income.IncomeItem;
import com.mj.matddakapi.model.income.IncomeResponse;
import com.mj.matddakapi.model.statistics.IncomeStatisticsItem;
import com.mj.matddakapi.model.statistics.IncomeStatisticsResponse;
import com.mj.matddakapi.repository.IncomeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class IncomeService {
    private final IncomeRepository incomeRepository;

    /**
     * 실적 복수 최신순 페이징
     */
    public ListResult<IncomeItem> getIncomesP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Income> incomes = incomeRepository.findAllByOrderByIdDesc(pageRequest);

        List<IncomeItem> response = new LinkedList<>();
        for (Income income : incomes) response.add(new IncomeItem.Builder(income).build());

        return ListConvertService.settingResult(response, incomes.getTotalElements(), incomes.getTotalPages(), incomes.getPageable().getPageNumber());
    }

    /**
     * 실적 단수
     */
    public IncomeResponse getIncome(long incomeId) {
        Income originData = incomeRepository.findById(incomeId).orElseThrow();

        return new IncomeResponse.Builder(originData).build();
    }

    /**
     * 관리자 일간 수익 단수
     */
    public IncomeStatisticsResponse getDateIncomeAdmin() {
        LocalDate today = LocalDate.now();
        List<Income> originList = incomeRepository.findAllByDateIncome(today);
        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        double totalDay = 0D;
        for (Income income : originList){
            totalDay += income.getFeeAdmin();
        }

        response.setTotalAdmin(totalDay);

        return response;
    }

    /**
     * 라이더 일간 수익 단수
     */
    public IncomeStatisticsResponse getDateIncomeRider(long riderId) {
        LocalDate today = LocalDate.now();
        List<Income> originList = incomeRepository.findAllByDateIncomeAndDelivery_Rider_Id(today, riderId);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        int totalCount = originList.size();
        double totalFeeDay = 0D;
        double totalFeeAdmin = 0D;
        double totalFeeRider = 0D;
        double totalDistance = 0D;
        for (Income income : originList){
            totalFeeDay += income.getFeeTotal();
            totalFeeAdmin += income.getFeeAdmin();
            totalFeeRider += income.getFeeRider();
            totalDistance += (double) Math.round(income.getDelivery().getAsk().getDistance() * 1000) / 1000;
        }
        double perDistance = (double) Math.round(totalDistance / totalCount * 1000) / 1000;

        response.setTotalCount(totalCount);
        response.setTotalFee(totalFeeDay);
        response.setTotalAdmin(totalFeeAdmin);
        response.setTotalRider(totalFeeRider);
        response.setTotalDistance(totalDistance);
        response.setPerDistance(perDistance);

        return response;
    }

    /**
     * 관리자 주간 수익 단수
     */
    public IncomeStatisticsResponse getWeekIncomeAdmin() {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(6);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetween(startDay, today);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        double totalWeek = 0D;
        for (Income income : originList){
            totalWeek += income.getFeeAdmin();
        }

        response.setTotalAdmin(totalWeek);

        return response;
    }

    /**
     * 라이더 주간 수익 단수
     */
    public IncomeStatisticsResponse getWeekIncomeRider(long riderId) {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(6);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(startDay, today, riderId);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        int totalCount = originList.size();
        double totalFeeWeek = 0D;
        double totalFeeAdmin = 0D;
        double totalFeeRider = 0D;
        double totalDistance = 0D;
        for (Income income : originList){
            totalFeeWeek += income.getFeeTotal();
            totalFeeAdmin += income.getFeeAdmin();
            totalFeeRider += income.getFeeRider();
            totalDistance += (double) Math.round(income.getDelivery().getAsk().getDistance() * 1000) / 1000;
        }
        double perDistance = (double) Math.round(totalDistance / totalCount * 1000) / 1000;

        response.setTotalCount(totalCount);
        response.setTotalFee(totalFeeWeek);
        response.setTotalAdmin(totalFeeAdmin);
        response.setTotalRider(totalFeeRider);
        response.setTotalDistance(totalDistance);
        response.setPerDistance(perDistance);

        return response;
    }

    /**
     * 관리자 월간 수익 단수
     */
    public IncomeStatisticsResponse getMonthIncomeAdmin() {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(29);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetween(startDay, today);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        double totalMonth = 0D;
        for (Income income : originList){
            totalMonth += income.getFeeAdmin();
        }

        response.setTotalAdmin(totalMonth);

        return response;
    }

    /**
     * 라이더 월간 수익 단수
     */
    public IncomeStatisticsResponse getMonthIncomeRider(long riderId) {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(29);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(startDay, today, riderId);
        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        int totalCount = originList.size();
        double totalFeeMonth = 0D;
        double totalFeeAdmin = 0D;
        double totalFeeRider = 0D;
        double totalDistance = 0D;
        for (Income income : originList){
            totalFeeMonth += income.getFeeTotal();
            totalFeeAdmin += income.getFeeAdmin();
            totalFeeRider += income.getFeeRider();
            totalDistance += (double) Math.round(income.getDelivery().getAsk().getDistance() * 1000) / 1000;
        }
        double perDistance = (double) Math.round(totalDistance / totalCount * 1000) / 1000;

        response.setTotalCount(totalCount);
        response.setTotalFee(totalFeeMonth);
        response.setTotalAdmin(totalFeeAdmin);
        response.setTotalRider(totalFeeRider);
        response.setTotalDistance(totalDistance);
        response.setPerDistance(perDistance);

        return response;
    }
}


///**
// * 라이더 주간 수익 복수
// */
// 3차 시도
//    public List<IncomeStatisticsItem> getWeekIncomesRider(long riderId) {
//        LocalDate today = LocalDate.now();
//        LocalDate startDay = today.minusDays(6);
//        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(startDay, today, riderId);
//        List<IncomeStatisticsItem> result = new LinkedList<>();
//
//        // 7일간의 수익을 저장하는 배열
//        double[] totalWeek = new double[7];
//
//        for (Income income : originList) {
//            LocalDate incomeDate = income.getDateIncome();
//            // 오늘부터 수입이 발생한 날짜까지의 차이를 인덱스로 사용
//            int index = (int) (today.toEpochDay() - incomeDate.toEpochDay());
//            // 해당 날짜의 수익을 더함
//            totalWeek[index] += income.getFeeRider();
//        }
//
//        // 결과 리스트에 각 날짜별 수익을 추가
//        for (int i = 0; i < 7; i++) {
//            LocalDate date = today.minusDays(i);
//            result.add(new IncomeStatisticsItem(date, totalWeek[i]));
//        }
//
//        return result;
//    }

// 2차 시도
//for (Income income : originList) {
//IncomeStatisticsItem response = new IncomeStatisticsItem();
//totalWeek += income.getFeeRider();
//
//            switch () {
//                    case today: totalToday += income.getFeeRider();
//                case agoOneDay: totalAgoOneDay += income.getFeeRider();
//                case agoTwoDay: totalAgoTwoDay += income.getFeeRider();
//                case agoThreeDay: totalAgoThreeDay += income.getFeeRider();
//                case agoFourDay: totalAgoFourDay += income.getFeeRider();
//                case agoFiveDay: totalAgoFiveDay += income.getFeeRider();
//                case startDay: totalStartDay += income.getFeeRider();
//default:
//        break;
//        }
//        result.add(response);
//        }
//                return result;

// 1차 시도
//for (Income income : originList) {
//totalWeek += income.getFeeRider();
//
//            if (income.equals(today)) {
//totalToday += income.getFeeRider();
//totalAgoOneDay += income.getFeeRider();
//totalAgoTwoDay += income.getFeeRider();
//totalAgoThreeDay += income.getFeeRider();
//totalAgoFourDay += income.getFeeRider();
//totalAgoFiveDay += income.getFeeRider();
//totalStartDay += income.getFeeRider();
//            } else if (income.equals(agoOneDay)) {
//totalAgoOneDay += income.getFeeRider();
//totalAgoTwoDay += income.getFeeRider();
//totalAgoThreeDay += income.getFeeRider();
//totalAgoFourDay += income.getFeeRider();
//totalAgoFiveDay += income.getFeeRider();
//totalStartDay += income.getFeeRider();
//            } else if (income.equals(agoTwoDay)) {
//totalAgoTwoDay += income.getFeeRider();
//totalAgoThreeDay += income.getFeeRider();
//totalAgoFourDay += income.getFeeRider();
//totalAgoFiveDay += income.getFeeRider();
//totalStartDay += income.getFeeRider();
//            } else if (income.equals(agoThreeDay)) {
//totalAgoThreeDay += income.getFeeRider();
//totalAgoFourDay += income.getFeeRider();
//totalAgoFiveDay += income.getFeeRider();
//totalStartDay += income.getFeeRider();
//            } else if (income.equals(agoFourDay)) {
//totalAgoFourDay += income.getFeeRider();
//totalAgoFiveDay += income.getFeeRider();
//totalStartDay += income.getFeeRider();
//            } else if (income.equals(agoFiveDay)) {
//totalAgoFiveDay += income.getFeeRider();
//totalStartDay += income.getFeeRider();
//            } else if (income.equals(startDay)) {
//totalStartDay += income.getFeeRider();
//            }
//                    }
//                    return result;