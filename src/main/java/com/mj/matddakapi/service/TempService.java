package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.lib.CommonFile;
import com.mj.matddakapi.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class TempService {
    private final StoreRepository storeRepository;

    /**
     *
     * @param multipartFile 데이터 베이스에 가게 정보 한번에 업로드
     * @throws IOException
     */


    public void setStoreByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // 버퍼에서 넘어온 string 한줄을 임시로 담아 둘 변수
        String line = "";
        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1씩 증가해서 기록 해 둘 변수
        int index = 0;

        // 빈 값이 아닐 때까지 넣겠다
        while ((line = bufferedReader.readLine()) != null){
            if (index > 0) {
                String[] cols = line.split(",");
                if (cols.length == 6){

                    // db에 넣기
                    storeRepository.save(new Store.BuilderPer(cols).build());
                }
            }
            index++;
        }

        bufferedReader.close();
    }
}
