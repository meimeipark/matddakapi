package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Board;
import com.mj.matddakapi.model.board.BoardChangeRequest;
import com.mj.matddakapi.model.board.BoardCreateRequest;
import com.mj.matddakapi.model.board.BoardItem;
import com.mj.matddakapi.model.board.BoardResponse;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    /**
     * 게시글 등록
     * @param request
     */
    public void setBoard(BoardCreateRequest request) {
        boardRepository.save(new Board.Builder(request).build());
    }

    /**
     * 게시글 복수 최신순
     * @return
     */
    public ListResult<BoardItem> getBoards() {
        List<Board> originList = boardRepository.findAllByOrderByIdDesc();
        List<BoardItem> result = new LinkedList<>();
        
        for (Board board : originList) result.add(new BoardItem.Builder(board).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 게시글 복수 최신순 페이징
     * @param pageNum
     * @return
     */
    public ListResult<BoardItem> getBoardsP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Board> boards = boardRepository.findAllByOrderByIdDesc(pageRequest);
        
        List<BoardItem> response = new LinkedList<>();
        for (Board board : boards) response.add(new BoardItem.Builder(board).build());
        
        return ListConvertService.settingResult(response, boards.getTotalElements(), boards.getTotalPages(), boards.getPageable().getPageNumber());
    }

    /**
     * 게시글 단수
     */
    public BoardResponse getBoard(long boardId) {
        Board originData = boardRepository.findById(boardId).orElseThrow();

        return new BoardResponse.Builder(originData).build();
    }

    /**
     * 게시글 수정
     */
    public void putBoard(long boardId, BoardChangeRequest request) {
        Board originData = boardRepository.findById(boardId).orElseThrow();

        originData.putBoard(request);
        boardRepository.save(originData);
    }

    /**
     * 게시글 삭제
     */
    public void delBoard(long boardId) {
        boardRepository.deleteById(boardId);
    }
}
