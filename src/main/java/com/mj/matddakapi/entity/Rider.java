package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.rider.*;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.rider.RiderChangeRequest;
import com.mj.matddakapi.model.rider.RiderJoinRequest;
import com.mj.matddakapi.model.rider.person.RiderBankChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
// rider 상세 구현
public class Rider implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Admin admin;

    @Column(nullable = false, length = 14)
    private String idNum;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Email
    @Column(nullable = false, unique = true, length = 40)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, unique = true, length = 20)
    private String phoneNumber;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private PhoneType phoneType;

    @Column(nullable = false, length = 20)
    private String bankOwner;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private BankName bankName;

    @Column(nullable = false)
    private String bankIdNum;

    @Column(nullable = false, length = 30)
    private String bankNumber;

    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private AddressWish addressWish;

    @Column(nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private DriveType driveType;

    @Column(length = 20)
    private String driveNumber;

    @Column(nullable = false)
    private LocalDate dateJoin;

    @Column(nullable = false)
    private Boolean isBan;

    @Enumerated(value = EnumType.STRING)
    private ReasonBan reasonBan;

    private String dateBan;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(nullable = false)
    private Boolean agPrivacy;

    @Column(nullable = false)
    private Boolean agLocation;


    /**
     * 관리자: 회원정보 수정
     */
    public void putRiders(RiderChangeRequest request) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date today = new Date();

        this.admin = request.getAdmin();
        this.username = request.getUsername();
        this.phoneNumber = request.getPhoneNumber();
        this.phoneType = request.getPhoneType();
        this.bankName = request.getBankName();
        this.bankIdNum = request.getBankIdNum();
        this.bankOwner = request.getBankOwner();
        this.bankNumber = request.getBankNumber();
        this.addressWish = request.getAddressWish();
        this.driveType = request.getDriveType();
        this.driveNumber = request.getDriveNumber();
        this.isBan = request.getIsBan();
        this.reasonBan = request.getReasonBan();
        this.dateBan = dateFormat.format(today);
        this.etcMemo = request.getEtcMemo();
    }

    /**
     * 라이더: 개인정보 수정
     */
    public void putRider(RiderPersonChangeRequest request) {
        this.username = request.getUsername();
        this.password = request.getPassword();
        this.phoneNumber = request.getPhoneNumber();
        this.addressWish =request.getAddressWish();
    }

    /**
     * 라이더 : 계좌 수정
     */
    public void putBank(RiderBankChangeRequest request) {
        this.bankOwner = request.getBankOwner();
        this.bankName = request.getBankName();
        this.bankIdNum = request.getBankIdNum();
        this.bankNumber = request.getBankNumber();
    }

    public void putRidingType(RiderTypeChangeRequest request) {
        this.driveType = request.getDriveType();
        this.driveNumber = request.getDriveNumber();
    }



    public Rider(Builder builder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date today = new Date();

        this.name = builder.name;
        this.admin = builder.admin;
        this.idNum = builder.idNum;
        this.birthDate = builder.birthDate;
        this.username = builder.username;
        this.password = builder.password;
        this.phoneNumber = builder.phoneNumber;
        this.phoneType = builder.phoneType;
        this.bankOwner = builder.bankOwner;
        this.bankName = builder.bankName;
        this.bankIdNum = builder.bankIdNum;
        this.bankNumber = builder.bankNumber;
        this.addressWish = builder.addressWish;
        this.driveType = builder.driveType;
        this.driveNumber = builder.driveNumber;
        this.dateJoin = builder.dateJoin;
        this.isBan = builder.isBan;
        this.reasonBan = builder.reasonBan;
        this.dateBan = dateFormat.format(today);
        this.etcMemo = builder.etcMemo;
        this.agPrivacy = builder.agPrivacy;
        this.agLocation = builder.agLocation;
    }


    // 권한(싱글톤)
    // 싱글톤 패턴은 특정 클래스의 인스턴스를 1개만 생성되는 것을 보장하는 디자인 패턴
    // 회원 1명 당 권한 그룹은 1개 -> 권한은 1개 입니다.
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(admin.toString()));
    }


    // 만료가 되었는지 안되었는지(유효기간)
    // 만료 안되었니? -> true 만료 안되었음
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // 잠기지 않았니? -> true
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // 신원 인증 만료 안되었니? -> true
    // 개인에 대한 신변 확인
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // 휴면회원 -> 활동성이 없음(활성화 개념이 없어) -> true
    @Override
    public boolean isEnabled() {
        return true;
    }


    public static class Builder implements CommonModelBuilder<Rider> {
        private final String name;
        private final Admin admin;
        private final String idNum;
        private final LocalDate birthDate;
        private final String username;
        private final String password;
        private final String phoneNumber;
        private final PhoneType phoneType;
        private final String bankOwner;
        private final BankName bankName;

        private final String bankIdNum;

        private final String bankNumber;
        private final AddressWish addressWish;
        private final DriveType driveType;
        private final String driveNumber;
        private final LocalDate dateJoin;
        private final Boolean isBan;
        private final ReasonBan reasonBan;

        private  String etcMemo;

        private String dateBan;

        private final Boolean agPrivacy;
        private final Boolean agLocation;


        public Builder(Admin admin, RiderJoinRequest request){


//            this.name = request.getName();
//            this.admin = admin;
//            this.idNum = request.getIdNum();
//            this.birthDate = request.getBirthDay();
//            this.username = request.getUsername();
//            this.password = request.getPassword();
//            this.phoneNumber = request.getPhoneNumber();
//            this.phoneType = request.getPhoneType();
//            this.bankOwner = request.getBankOwner();
//            this.bankName = request.getBankName();
//            this.bankIdNum = request.getBankIdNum();
//            this.bankNumber = request.getBankNumber();
//            this.addressWish = request.getAddressWish();
//            this.driveType = request.getDriveType();
//            this.driveNumber = request.getDriveNumber();
//            this.dateJoin = LocalDate.now();
//            this.agPrivacy = request.getAgPrivacy();
//            this.agLocation = request.getAgLocation();
//            this.isBan = false;
//            this.reasonBan = ReasonBan.NOT_BAN;





            LocalDate date1 = LocalDate.now();
            date1 = date1.minusYears(18);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date today = new Date();

            /**
             * 주민번호 입력시 생년월일로 치환하는 로직 s
             */

            String brithYear = request.getIdNum().substring(0, 2);
            String birthMonth = request.getIdNum().substring(2, 4);
            String birthDay = request.getIdNum().substring(4, 6);

            String genderCode = request.getIdNum().substring(6, 7);

            if (genderCode.equals("1") || genderCode.equals("2")) {
                brithYear = "19" + brithYear;
            } else {
                brithYear = "20" + brithYear;
            }
            String birth = brithYear + "-" + birthMonth + "-" + birthDay;
            LocalDate birthDate = LocalDate.parse(birth);

            /**
             * 주민번호 입력시 생년월일로 치환하는 로직 e
             */

            if (birthDate.isBefore(date1)) { // 생년월일이 date1 보다 전일때 실행
                this.name = request.getName();
                this.admin = admin;
                this.idNum = request.getIdNum();
                this.birthDate = birthDate;
                this.username = request.getUsername();
                this.password = request.getPassword();
                this.phoneNumber = request.getPhoneNumber();
                this.phoneType = request.getPhoneType();
                this.bankOwner = request.getBankOwner();
                this.bankName = request.getBankName();
                this.bankIdNum = request.getBankIdNum();
                this.bankNumber = request.getBankNumber();
                this.addressWish = request.getAddressWish();
                this.driveType = request.getDriveType();
                this.driveNumber = request.getDriveNumber();
                this.dateJoin = LocalDate.now();
                this.isBan = false;
                this.reasonBan = ReasonBan.NOT_BAN;
                this.agPrivacy = true;
                this.agLocation = true;

            } else {
                this.name = request.getName();
                this.admin = admin;
                this.idNum = request.getIdNum();
                this.birthDate = birthDate;
                this.username = request.getUsername();
                this.password = request.getPassword();
                this.phoneNumber = request.getPhoneNumber();
                this.phoneType = request.getPhoneType();
                this.bankOwner = request.getBankOwner();
                this.bankName = request.getBankName();
                this.bankIdNum = request.getBankIdNum();
                this.bankNumber = request.getBankNumber();
                this.addressWish = request.getAddressWish();
                this.driveType = request.getDriveType();
                this.driveNumber = request.getDriveNumber();
                this.dateJoin = LocalDate.now();
                this.isBan = true;
                this.reasonBan = ReasonBan.PARENTAL_CONSENT;
                this.dateBan = toString();
                this.agPrivacy = true;
                this.agLocation = true;
            }
        }
        @Override
        public Rider build() {
            return new Rider(this);
        }
    }
}

