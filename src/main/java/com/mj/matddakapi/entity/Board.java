package com.mj.matddakapi.entity;

import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.board.BoardChangeRequest;
import com.mj.matddakapi.model.board.BoardCreateRequest;
import jakarta.persistence.*;
import lombok.*;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String title;

    @Column(length = 100)
    private String img;

    private String text;

    @Column(nullable = false)
    private String dateCreate;

    public void putBoard(BoardChangeRequest request) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();

        this.title = request.getTitle();
        this.img = request.getImg();
        this.text = request.getText();
        this.dateCreate = dateFormat.format(today);
    }

    public Board(Builder builder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();

        this.title = builder.title;
        this.img = builder.img;
        this.text = builder.text;
        this.dateCreate = dateFormat.format(today);
    }

    public static class Builder implements CommonModelBuilder<Board> {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();
        private final String title;
        private String img;
        private String text;
        private final String dateCreate;

        public Builder(BoardCreateRequest request) {
            this.title = request.getTitle();
            this.img = request.getImg();
            this.text = request.getText();
            this.dateCreate = toString();
        }
        @Override
        public Board build() {
            return new Board(this);
        }
    }
}
