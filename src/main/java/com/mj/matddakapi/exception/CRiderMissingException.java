package com.mj.matddakapi.exception;

public class CRiderMissingException extends RuntimeException{
    public CRiderMissingException(String msg, Throwable t) {
        super(msg, t);
    }

    public CRiderMissingException(String msg) {
        super(msg);
    }

    public CRiderMissingException() {
        super();
    }
}
