package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.payhistory.PayHistoryCreateRequest;
import com.mj.matddakapi.service.PayHistoryService;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.RiderPayService;
import com.mj.matddakapi.service.RiderService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pay-history")
public class PayHistoryController {
    private final PayHistoryService payHistoryService;
    private final RiderService riderService;

    @PostMapping("/in/rider-id/{riderId}")
    @Operation(summary = "페이 증가")
    public CommonResult setPayIn(@PathVariable long riderId, @RequestBody PayHistoryCreateRequest request) {
        Rider rider = riderService.getData(riderId);
        payHistoryService.setPayIn(rider, request);

        return ResponseService.getSuccessResult();
    }

    @PostMapping("/out/rider-id/{riderId}")
    @Operation(summary = "페이 감소")
    public CommonResult setPayOut(@PathVariable long riderId, @RequestBody PayHistoryCreateRequest request) throws Exception {
        Rider rider = riderService.getData(riderId);
        payHistoryService.setPayOut(rider, request);

        return ResponseService.getSuccessResult();
    }
}
