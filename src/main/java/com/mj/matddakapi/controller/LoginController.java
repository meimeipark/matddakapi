package com.mj.matddakapi.controller;

import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.rider.login.LoginRequest;
import com.mj.matddakapi.model.rider.login.LoginResponse;
import com.mj.matddakapi.service.LoginService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

//    @PostMapping("/rider")
//    @Operation(summary = "로그인")
//    public SingleResult<LoginResponse> doLogin(@RequestBody LoginRequest request) {
//        return loginService.doLogin(request);
//    }

    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(Admin.ROLE_ADMIN, loginRequest, "WEB"));
    }

    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(Admin.ROLE_NORMAL, loginRequest, "APP"));
    }
}
