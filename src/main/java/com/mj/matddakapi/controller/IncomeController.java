package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.income.IncomeItem;
import com.mj.matddakapi.model.income.IncomeResponse;
import com.mj.matddakapi.model.statistics.IncomeStatisticsItem;
import com.mj.matddakapi.model.statistics.IncomeStatisticsResponse;
import com.mj.matddakapi.service.IncomeService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/income")
public class IncomeController {
    private final IncomeService incomeService;

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "실적 복수 최신순 페이징")
    public ListResult<IncomeItem> getIncomesP(@PathVariable int pageNum) {
        return ResponseService.getListResult(incomeService.getIncomesP(pageNum), true);
    }

    @GetMapping("/detail/{incomeId}")
    @Operation(summary = "실적 단수")
    public SingleResult<IncomeResponse> getIncome(@PathVariable long incomeId) {
        return ResponseService.getSingleResult(incomeService.getIncome(incomeId));
    }

    @GetMapping("/date/admin")
    @Operation(summary = "관리자 하루 수익")
    public SingleResult<IncomeStatisticsResponse> getDayIncomeAdmin() {
        return ResponseService.getSingleResult(incomeService.getDateIncomeAdmin());
    }

    @GetMapping("/date/rider/{riderId}")
    @Operation(summary = "라이더 하루 수익")
    public SingleResult<IncomeStatisticsResponse> getDateIncomeRider(@PathVariable long riderId) {
        return ResponseService.getSingleResult(incomeService.getDateIncomeRider(riderId));
    }

    @GetMapping("/week/admin")
    @Operation(summary = "관리자 주간 수익")
    public SingleResult<IncomeStatisticsResponse> getWeekIncomeAdmin() {
        return ResponseService.getSingleResult(incomeService.getWeekIncomeAdmin());
    }

    @GetMapping("/week/rider/{riderId}")
    @Operation(summary = "라이더 주간 수익")
    public SingleResult<IncomeStatisticsResponse> getWeekIncomeRider(@PathVariable long riderId) {
        return ResponseService.getSingleResult(incomeService.getWeekIncomeRider(riderId));
    }

    @GetMapping("/month/admin")
    @Operation(summary = "관리자 월간 수익")
    public SingleResult<IncomeStatisticsResponse> getMonthIncomeAdmin() {
        return ResponseService.getSingleResult(incomeService.getMonthIncomeAdmin());
    }

    @GetMapping("/month/rider/{riderId}")
    @Operation(summary = "라이더 월간 수익")
    public SingleResult<IncomeStatisticsResponse> getMonthIncomeRider(@PathVariable long riderId) {
        return ResponseService.getSingleResult(incomeService.getMonthIncomeRider(riderId));
    }

//    @GetMapping("/all/week/rider/{riderId}")
//    public ListResult<IncomeStatisticsItem> getWeekIncomesRider(@PathVariable long riderId) {
//        return ResponseService.getListResult(incomeService.getWeekIncomesRider(riderId));
//    }
}
