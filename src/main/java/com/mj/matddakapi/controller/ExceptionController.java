package com.mj.matddakapi.controller;

import com.mj.matddakapi.exception.CRiderMissingException;
import com.mj.matddakapi.model.generic.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 메세지를 처리해 주기 위한 컨트롤러
@RestController
@RequestMapping("/exception")
public class ExceptionController {

    // 핸들러로 던져주기
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException(){
        throw new CRiderMissingException();
    }

    // 권한에 대한 체크 에러
    @GetMapping("/entry-point")
    public CommonResult entryPointException(){
        throw new CRiderMissingException();
    }
}
