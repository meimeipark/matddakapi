package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.rider.*;
import com.mj.matddakapi.model.rider.person.RiderBankChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.RiderService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/rider")
public class RiderController {
    private final RiderService riderService;

    @PostMapping("/join") // 회원가입
    @Operation(summary = "회원가입")
    public CommonResult setRider(@RequestBody RiderJoinRequest request) throws Exception {
        riderService.setRider(Admin.ROLE_ADMIN, request);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all") //회원 리스트
    @Operation(summary = "회원 리스트 (관리자)")
    public ListResult<RiderItem> getRiders() {return ResponseService.getListResult(riderService.getRiders());}

    @GetMapping("/detail/{id}") // 개인 회원 상세 보기
    @Operation(summary = "개인 회원 상세보기 (관리자,라이더)")
    public SingleResult<RiderResponse> getRider(@PathVariable long id) {

        return ResponseService.getSingleResult(riderService.getRider(id));
    }

    @GetMapping("/all/{pageNum}") //라이더 페이징
    @Operation(summary = "라이더 리스트 페이징")
    public ListResult<RiderItem> getPages(@PathVariable int pageNum){
        return riderService.getPages(pageNum);
    }
    @PutMapping("/change/{id}") // 라이더 정보 수정
    @Operation(summary = "회원 정보 수정 (관리자)")
    public CommonResult putRiders(@PathVariable long id, @RequestBody RiderChangeRequest request){
        riderService.putRiders(id, request);

        return ResponseService.getSuccessResult();
    }
    @PutMapping("/person/{id}") // 라이더 개인정보 수정
    @Operation(summary = "개인 정보 수정 (라이더)")
    public CommonResult putRider(@PathVariable long id, @RequestBody RiderPersonChangeRequest request){
        riderService.putRider(id, request);

        return ResponseService.getSuccessResult();
    }
    @PutMapping("/bank/{id}")
    @Operation(summary = "라이더 계좌 수정 (라이더)")
    public CommonResult putBank(@PathVariable long id, @RequestBody RiderBankChangeRequest request){
        riderService.putBank(id, request);

        return ResponseService.getSuccessResult();
    }
    @PutMapping("/type/{id}")
    @Operation(summary = "라이더 운송수단 변경 (라이더)")
    public CommonResult putRidingType(@PathVariable long id, @RequestBody RiderTypeChangeRequest request){
        riderService.putRidingType(id, request);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/black-list/all")
    @Operation(summary = "라이더 블랙리스트")
    public ListResult<RiderItem> getBlackList(){
        return ResponseService.getListResult(riderService.getBlackList());
    }
}
