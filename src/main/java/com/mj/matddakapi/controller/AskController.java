package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.model.ask.AskCreateRequest;
import com.mj.matddakapi.model.ask.AskItem;
import com.mj.matddakapi.model.ask.AskResponse;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.service.AskService;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.StoreService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ask")
public class AskController {
    private final AskService askService;
    private final StoreService storeService;

    @PostMapping("/new/store-id/{storeId}")
    @Operation(summary = "주문 등록")
    public CommonResult setAsk(@PathVariable long storeId, @RequestBody AskCreateRequest request) {
        Store store = storeService.getData(storeId);
        askService.setAsk(store, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "주문 복수 최신순으로")
    public ListResult<AskItem> getAsks() {
        return ResponseService.getListResult(askService.getAsks(), true);
    }

    @GetMapping("/all/request")
    @Operation(summary = "주문 복수 요청상태만 최신순으로")
    public ListResult<AskItem> getAsksRe() {
        return ResponseService.getListResult(askService.getAsksRe(), true);
    }

    @GetMapping("/all/cancel/date")
    @Operation(summary = "주문 복수 취소 상태만 최신순 하루치")
    public ListResult<AskItem> getAsksCanDay() {
        return ResponseService.getListResult(askService.getAsksCanDay(), true);
    }

    @GetMapping("/detail/{askId}")
    @Operation(summary = "주문 단수")
    public SingleResult<AskResponse> getAsk(@PathVariable long askId) {
        return ResponseService.getSingleResult(askService.getAsk(askId));
    }

    @PutMapping("/change/cancel/{askId}")
    @Operation(summary = "주문 상태 취소로 변경")
    public CommonResult putAskCancel(@PathVariable long askId) {
        askService.putAskCancel(askId);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/request/{pageNum}")
    @Operation(summary = "주문 복수 요청 상태만 최신순으로 페이징")
    public ListResult<AskItem> getAskReP(@PathVariable int pageNum) {
        return ResponseService.getListResult(askService.getAskReP(pageNum), true);
    }

    @GetMapping("/all/cancel/{pageNum}")
    @Operation(summary = "주문 복수 취소 상태만 최신순으로 페이징")
    public ListResult<AskItem> getAskCanP(@PathVariable int pageNum) {
        return ResponseService.getListResult(askService.getAskCanP(pageNum), true);
    }

    @GetMapping("/all/request-cancel/{pageNum}")
    @Operation(summary = "주문 복수 요청 + 주문취소 상태만 최신순 페이징")
    public ListResult<AskItem> getAskReCanP(@PathVariable int pageNum) {
        return ResponseService.getListResult(askService.getAskReCanP(pageNum), true);
    }
}
