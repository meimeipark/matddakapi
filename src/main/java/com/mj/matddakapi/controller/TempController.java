package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.TempService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;

    /**
     *
     * @param csvFile 가게 정보 파일
     * @return 데이터 베이스 업로드
     * @throws IOException -
     */
    @PostMapping("/store/file-upload")
    @Operation(summary = "데이터 베이스 가게 정보 파일 업로드")
    public CommonResult setStoreByFile(@RequestParam("csvFile") MultipartFile csvFile) throws IOException {
        tempService.setStoreByFile(csvFile);

        return ResponseService.getSuccessResult();
    }

}
