package com.mj.matddakapi.lib;

public class CommonCheck {
    public static boolean isValidEmail(String username) {
        //email id는 4~19글자 까지 +@[도메인]+.{{com}}
        String pattern = "^[a-zA-Z0-9+-_.]{4,19}+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]{2,}$";
        return username.matches(pattern);
    }
}
