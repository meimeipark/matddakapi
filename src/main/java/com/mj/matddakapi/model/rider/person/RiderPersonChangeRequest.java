package com.mj.matddakapi.model.rider.person;

import com.mj.matddakapi.enums.rider.AddressWish;
import com.mj.matddakapi.enums.rider.PhoneType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RiderPersonChangeRequest {
    private String username;

    private String password;

    private String phoneNumber;

    private PhoneType phoneType;

    private AddressWish addressWish;
}
