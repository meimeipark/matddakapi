package com.mj.matddakapi.model.rider.login;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.service.LoginService;
import com.mj.matddakapi.service.RiderService;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.yaml.snakeyaml.tokens.Token;

import java.security.Provider;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    private String token;
    private String name;

    private LoginResponse(Builder builder) {
        this.token = builder.token;
        this.name = builder.name;
    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {
        private final String token;
        private final String name;

        public Builder(String token, String name){
            this.token = token;
            this.name = name;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
