package com.mj.matddakapi.model.rider.person;

import com.mj.matddakapi.enums.rider.BankName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RiderBankChangeRequest {
    private String bankOwner;

    private BankName bankName;

    private String bankNumber;

    private String bankIdNum;
}
