package com.mj.matddakapi.model.rider;

import com.mj.matddakapi.enums.rider.*;
import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter

public class RiderJoinRequest {
    private String name;

    private String idNum;

    private String username;

    private String password;

    private String passwordRe;

    private String phoneNumber;

    private PhoneType phoneType;

    private String bankOwner;

    private BankName bankName;

    private String bankIdNum;

    private LocalDate birthDay;

    private String bankNumber;

    private AddressWish addressWish;

    private DriveType driveType;

    private String driveNumber;

    private Boolean agPrivacy;

    private Boolean agLocation;


}
