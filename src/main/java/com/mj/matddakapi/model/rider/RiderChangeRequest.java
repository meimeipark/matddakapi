package com.mj.matddakapi.model.rider;

import com.mj.matddakapi.enums.rider.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class RiderChangeRequest {
    private Admin Admin;

    private String username;

    private String password;

    private String phoneNumber;

    private PhoneType phoneType;

    private String bankOwner;

    private BankName bankName;

    private String bankIdNum;

    private String bankNumber;

    private AddressWish addressWish;

    private DriveType driveType;

    private String driveNumber;

    private Boolean isBan;

    private ReasonBan reasonBan;

    private LocalDateTime dateBan;

    private String etcMemo;
}
