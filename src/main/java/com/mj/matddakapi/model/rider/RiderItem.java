package com.mj.matddakapi.model.rider;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.*;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)

public class RiderItem {
    private Long id;

    private String name;

    private String admin;

    private String idNum;

    private LocalDate birthDate;

    private String username;

    private String password;

    private String phoneNumber;

    private String phoneType;

    private String bankOwner;

    private String bankName;

    private String bankIdNum;

    private String bankNumber;

    private String addressWish;

    private String driveType;

    private String driveNumber;

    private LocalDate dateJoin;

    private String isBan;

    private String reasonBan;

    private String dateBan;

    private String etcMemo;

    private String agPrivacy;

    private String agLocation;

    private RiderItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.admin = builder.admin;
        this.idNum = builder.idNum;
        this.birthDate = builder.birthDate;
        this.username = builder.username;
        this.password = builder.password;
        this.phoneNumber = builder.phoneNumber;
        this.phoneType = builder.phoneType;
        this.bankOwner = builder.bankOwner;
        this.bankName = builder.bankName;
        this.bankIdNum = builder.bankIdNum;
        this.bankNumber = builder.bankNumber;
        this.addressWish = builder.addressWish;
        this.driveType = builder.driveType;
        this.driveNumber = builder.driveNumber;
        this.dateJoin = builder.dateJoin;
        this.isBan = builder.isBan;
        this.reasonBan = builder.reasonBan;
        this.dateBan = builder.dateBan;
        this.etcMemo = builder.etcMemo;
        this.agPrivacy = builder.agPrivacy;
        this.agLocation = builder.agLocation;
    }


    public static class Builder implements CommonModelBuilder<RiderItem> {
        private final Long id;

        private final String name;

        private final String admin;

        private final String idNum;

        private final LocalDate birthDate;

        private final String username;

        private final String password;

        private final String phoneNumber;

        private final String phoneType;

        private final String bankOwner;

        private final String bankName;

        private final String bankIdNum;

        private final String bankNumber;

        private final String addressWish;

        private final String driveType;

        private final String driveNumber;

        private final LocalDate dateJoin;

        private final String isBan;

        private final String reasonBan;

        private final String dateBan;

        private final String etcMemo;

        private final String agPrivacy;

        private final String agLocation;

        public Builder(Rider rider) {
            this.id = rider.getId();
            this.name = rider.getName();
            this.admin = rider.getAdmin().getName();
            this.idNum = rider.getIdNum();
            this.birthDate = rider.getBirthDate();
            this.username = rider.getUsername();
            this.password = rider.getPassword();
            this.phoneNumber = rider.getPhoneNumber();
            this.phoneType = rider.getPhoneType().getName();
            this.bankOwner = rider.getBankOwner();
            this.bankName = rider.getBankName().getName();
            this.bankNumber = rider.getBankNumber();
            this.bankIdNum = rider.getBankIdNum();
            this.addressWish = rider.getAddressWish().getName();
            this.driveType = rider.getDriveType().getName();
            this.driveNumber = rider.getDriveNumber();
            this.dateJoin = rider.getDateJoin();
            this.isBan = rider.getIsBan() ? "정지" : "활동중";
            this.reasonBan = rider.getReasonBan().getName();
            this.dateBan = rider.getDateBan();
            this.etcMemo = rider.getEtcMemo();
            this.agPrivacy = rider.getAgPrivacy() ? "동의함" : "동의하지 않음";
            this.agLocation = rider.getAgLocation() ? "동의함" : "동의하지 않음";

        }

        @Override
        public RiderItem build() {
            return new RiderItem(this);
        }
    }
}
