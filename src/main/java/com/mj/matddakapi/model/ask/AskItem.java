package com.mj.matddakapi.model.ask;

import com.mj.matddakapi.entity.Ask;
import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.enums.ask.HowPay;
import com.mj.matddakapi.enums.ask.StateAsk;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AskItem {
    private Long id;
    private Long storeId;
    private String storeName;
    private String stateAsk;
    private LocalDate dateAsk;
    private String timeAsk;
    private String askMenu;
    private Double priceFood;
    private Double priceRide;
    private Double priceTotal;
    private String isPay;
    private String howPay;
    private String requestStore;
    private String requestRider;
    private String phoneClient;
    private String addressStoreDo;
    private String addressClientDo;
    private String addressClientG;
    private Double xClient;
    private Double yClient;
    private Double distance;

    public AskItem(Builder builder) {
        this.id = builder.id;
        this.storeId = builder.storeId;
        this.storeName = builder.storeName;
        this.stateAsk = builder.stateAsk;
        this.dateAsk = builder.dateAsk;
        this.timeAsk = builder.timeAsk;
        this.askMenu = builder.askMenu;
        this.priceFood = builder.priceFood;
        this.priceRide = builder.priceRide;
        this.priceTotal = builder.priceTotal;
        this.isPay = builder.isPay;
        this.howPay = builder.howPay;
        this.requestStore = builder.requestStore;
        this.requestRider = builder.requestRider;
        this.phoneClient = builder.phoneClient;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressClientDo = builder.addressClientDo;
        this.addressClientG = builder.addressClientG;
        this.xClient = builder.xClient;
        this.yClient = builder.yClient;
        this.distance = builder.distance;
    }

    public static class Builder implements CommonModelBuilder<AskItem> {
        private final Long id;
        private final Long storeId;
        private final String storeName;
        private final String stateAsk;
        private final LocalDate dateAsk;
        private final String timeAsk;
        private final String askMenu;
        private final Double priceFood;
        private final Double priceRide;
        private final Double priceTotal;
        private final String isPay;
        private final String howPay;
        private final String requestStore;
        private final String requestRider;
        private final String phoneClient;
        private final String addressStoreDo;
        private final String addressClientDo;
        private final String addressClientG;
        private final Double xClient;
        private final Double yClient;
        private final Double distance;

        public Builder(Ask ask) {
            this.id = ask.getId();
            this.storeId = ask.getStore().getId();
            this.storeName = ask.getStore().getStoreName();
            this.stateAsk = ask.getStateAsk().getName();
            this.dateAsk = ask.getDateAsk();
            this.timeAsk = ask.getTimeAsk();
            this.askMenu = ask.getAskMenu();
            this.priceFood = ask.getPriceFood();
            this.priceRide = ask.getPriceRide();
            this.priceTotal = ask.getPriceTotal();
            this.isPay = ask.getIsPay() ? "선" : "후";
            this.howPay = ask.getHowPay().getName();
            this.requestStore = ask.getRequestStore();
            this.requestRider = ask.getRequestRider();
            this.phoneClient = ask.getPhoneClient();
            this.addressStoreDo = ask.getStore().getAddressStoreDo();
            this.addressClientDo = ask.getAddressClientDo();
            this.addressClientG = ask.getAddressClientG();
            this.xClient = ask.getXClient();
            this.yClient = ask.getYClient();
            this.distance = ask.getDistance();
        }

        @Override
        public AskItem build() {
            return new AskItem(this);
        }
    }
}
