package com.mj.matddakapi.model.attendance;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.enums.attendance.StateWork;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Optional;

@Getter
@NoArgsConstructor
public class AttendanceResponse {
    private Long id;
    private Long riderId;
    private String riderName;
    private StateWork stateWork;
    private LocalDateTime dateCheckWork;

    public AttendanceResponse(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.stateWork = builder.stateWork;
        this.dateCheckWork = builder.dateCheckWork;
    }

    public static class Builder implements CommonModelBuilder<AttendanceResponse> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final StateWork stateWork;
        private final LocalDateTime dateCheckWork;

        public Builder(Attendance attendance) {
            this.id = attendance.getId();
            this.riderId = attendance.getRider().getId();
            this.riderName = attendance.getRider().getName();
            this.stateWork = attendance.getStateWork();
            this.dateCheckWork = attendance.getDateCheckWork();
        }
        @Override
        public AttendanceResponse build() {
            return new AttendanceResponse(this);
        }
    }
}
