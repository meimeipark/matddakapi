package com.mj.matddakapi.model.board;

import com.mj.matddakapi.entity.Board;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardItem {
    private Long id;
    private String title;
    private String img;
    private String text;
    private String dateCreate;

    public BoardItem(Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.img = builder.img;
        this.text = builder.text;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<BoardItem> {
        private final Long id;
        private final String title;
        private final String img;
        private final String text;
        private final String dateCreate;

        public Builder(Board board) {
            this.id = board.getId();
            this.title = board.getTitle();
            this.img = board.getImg();
            this.text = board.getText();
            this.dateCreate = board.getDateCreate();
        }
        @Override
        public BoardItem build() {
            return new BoardItem(this);
        }
    }
}
