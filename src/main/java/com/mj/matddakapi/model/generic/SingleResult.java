package com.mj.matddakapi.model.generic;

import com.mj.matddakapi.model.generic.CommonResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult {
    private T data;
}
