package com.mj.matddakapi.model.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private String msg;
    private Integer code;
    private Boolean isSuccess;
}
