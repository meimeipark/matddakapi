package com.mj.matddakapi.model.statistics;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncomeStatisticsResponse {
    private Integer totalCount;
    private Double totalFee;
    private Double totalAdmin;
    private Double totalRider;
    private Double totalDistance;
    private Double perDistance;
}
