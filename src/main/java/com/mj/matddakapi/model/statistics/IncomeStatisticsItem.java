package com.mj.matddakapi.model.statistics;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class IncomeStatisticsItem {
    private List<Double> totalFee;
}
